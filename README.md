## Setup local env
```bash
docker-compose up -d
docker-compose exec mysql mysql -uroot -ppassword > create database spx
docker-compose exec php composer install
docker-compose exec php php bin/console doctrine:migrations:migrate
```
